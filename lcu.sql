--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.2
-- Dumped by pg_dump version 9.6.2

-- Started on 2017-04-19 16:20:33

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE "LCU";
--
-- TOC entry 2284 (class 1262 OID 16426)
-- Name: LCU; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE "LCU" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Chile.1252' LC_CTYPE = 'Spanish_Chile.1252';


ALTER DATABASE "LCU" OWNER TO postgres;

\connect "LCU"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2285 (class 1262 OID 16426)
-- Dependencies: 2284
-- Name: LCU; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE "LCU" IS 'Base de Datos Liceo de Niñas Corina Urbina';


--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2287 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 202 (class 1259 OID 16519)
-- Name: Alumno_Curso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "Alumno_Curso" (
    id integer NOT NULL,
    id_alumno integer NOT NULL,
    id_curso integer NOT NULL,
    nr_ano integer NOT NULL,
    tx_estado character varying(1) NOT NULL,
    id_usuario integer NOT NULL,
    fc_modificacion timestamp without time zone
);


ALTER TABLE "Alumno_Curso" OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16517)
-- Name: Alumno_Curso_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "Alumno_Curso_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Alumno_Curso_id_seq" OWNER TO postgres;

--
-- TOC entry 2288 (class 0 OID 0)
-- Dependencies: 201
-- Name: Alumno_Curso_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "Alumno_Curso_id_seq" OWNED BY "Alumno_Curso".id;


--
-- TOC entry 204 (class 1259 OID 16537)
-- Name: Alumno_Prueba; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "Alumno_Prueba" (
    id integer NOT NULL,
    id_alumno integer NOT NULL,
    id_prueba integer NOT NULL,
    nr_puntaje_obtenido integer NOT NULL,
    nr_nota integer NOT NULL,
    tx_estado character varying(1) NOT NULL,
    id_usuario integer NOT NULL,
    fc_modificacion timestamp without time zone
);


ALTER TABLE "Alumno_Prueba" OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 16535)
-- Name: Alumno_Prueba_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "Alumno_Prueba_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Alumno_Prueba_id_seq" OWNER TO postgres;

--
-- TOC entry 2289 (class 0 OID 0)
-- Dependencies: 203
-- Name: Alumno_Prueba_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "Alumno_Prueba_id_seq" OWNED BY "Alumno_Prueba".id;


--
-- TOC entry 192 (class 1259 OID 16456)
-- Name: Alumnos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "Alumnos" (
    id integer NOT NULL,
    nr_rut integer NOT NULL,
    tx_dv character varying(1) NOT NULL,
    tx_nombre character varying(100) NOT NULL,
    tx_apellido_paterno character varying(100) NOT NULL,
    tx_apellido_materno character varying(100) NOT NULL,
    tx_estado character varying(1) NOT NULL,
    id_usuario integer NOT NULL,
    fc_modificacion timestamp without time zone
);


ALTER TABLE "Alumnos" OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 16454)
-- Name: Alumnos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "Alumnos_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Alumnos_id_seq" OWNER TO postgres;

--
-- TOC entry 2290 (class 0 OID 0)
-- Dependencies: 191
-- Name: Alumnos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "Alumnos_id_seq" OWNED BY "Alumnos".id;


--
-- TOC entry 188 (class 1259 OID 16440)
-- Name: Asignatura; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "Asignatura" (
    id integer NOT NULL,
    tx_nombre character varying(100) NOT NULL,
    tx_estado character varying(1) NOT NULL,
    id_usuario integer NOT NULL,
    fc_modificacion timestamp without time zone
);


ALTER TABLE "Asignatura" OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 16501)
-- Name: Asignatura_Curso; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "Asignatura_Curso" (
    id integer NOT NULL,
    id_asignatura integer NOT NULL,
    id_curso integer NOT NULL
);


ALTER TABLE "Asignatura_Curso" OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 16499)
-- Name: Asignatura_Curso_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "Asignatura_Curso_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Asignatura_Curso_id_seq" OWNER TO postgres;

--
-- TOC entry 2291 (class 0 OID 0)
-- Dependencies: 199
-- Name: Asignatura_Curso_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "Asignatura_Curso_id_seq" OWNED BY "Asignatura_Curso".id;


--
-- TOC entry 198 (class 1259 OID 16485)
-- Name: Asignatura_Docente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "Asignatura_Docente" (
    id integer NOT NULL,
    id_docente integer,
    id_asignatura integer
);


ALTER TABLE "Asignatura_Docente" OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16556)
-- Name: Asignatura_Prueba; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "Asignatura_Prueba" (
    id integer NOT NULL,
    id_asignatura integer NOT NULL,
    id_prueba integer NOT NULL
);


ALTER TABLE "Asignatura_Prueba" OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 16554)
-- Name: Asignatura_Prueba_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "Asignatura_Prueba_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Asignatura_Prueba_id_seq" OWNER TO postgres;

--
-- TOC entry 2292 (class 0 OID 0)
-- Dependencies: 205
-- Name: Asignatura_Prueba_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "Asignatura_Prueba_id_seq" OWNED BY "Asignatura_Prueba".id;


--
-- TOC entry 197 (class 1259 OID 16483)
-- Name: Asignatura_docente_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "Asignatura_docente_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Asignatura_docente_id_seq" OWNER TO postgres;

--
-- TOC entry 2293 (class 0 OID 0)
-- Dependencies: 197
-- Name: Asignatura_docente_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "Asignatura_docente_id_seq" OWNED BY "Asignatura_Docente".id;


--
-- TOC entry 187 (class 1259 OID 16438)
-- Name: Asignatura_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "Asignatura_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Asignatura_id_seq" OWNER TO postgres;

--
-- TOC entry 2294 (class 0 OID 0)
-- Dependencies: 187
-- Name: Asignatura_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "Asignatura_id_seq" OWNED BY "Asignatura".id;


--
-- TOC entry 190 (class 1259 OID 16448)
-- Name: Cursos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "Cursos" (
    id integer NOT NULL,
    tx_curso character varying(10) NOT NULL,
    tx_descripcion character varying(100) NOT NULL,
    tx_estado character varying(1) NOT NULL,
    id_usuario integer NOT NULL,
    fc_modificacion timestamp without time zone
);


ALTER TABLE "Cursos" OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 16446)
-- Name: Cursos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "Cursos_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Cursos_id_seq" OWNER TO postgres;

--
-- TOC entry 2295 (class 0 OID 0)
-- Dependencies: 189
-- Name: Cursos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "Cursos_id_seq" OWNED BY "Cursos".id;


--
-- TOC entry 186 (class 1259 OID 16429)
-- Name: Docentes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "Docentes" (
    id integer NOT NULL,
    nr_rut integer NOT NULL,
    tx_dv character varying(1) NOT NULL,
    tx_nombre character varying(100) NOT NULL,
    tx_apellido_paterno character varying(100) NOT NULL,
    tx_apellido_materno character varying(100) NOT NULL,
    tx_estado bit varying(1)[] NOT NULL,
    id_usuario integer NOT NULL,
    fc_modificacion timestamp without time zone
);


ALTER TABLE "Docentes" OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 16427)
-- Name: Docentes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "Docentes_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Docentes_id_seq" OWNER TO postgres;

--
-- TOC entry 2296 (class 0 OID 0)
-- Dependencies: 185
-- Name: Docentes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "Docentes_id_seq" OWNED BY "Docentes".id;


--
-- TOC entry 210 (class 1259 OID 16592)
-- Name: Perfil; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "Perfil" (
    id integer NOT NULL,
    tx_descripcion character varying(100) NOT NULL,
    tx_estado character varying(1) NOT NULL,
    id_usuario integer NOT NULL,
    fc_modificacion timestamp without time zone
);


ALTER TABLE "Perfil" OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16590)
-- Name: Perfil_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "Perfil_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Perfil_id_seq" OWNER TO postgres;

--
-- TOC entry 2297 (class 0 OID 0)
-- Dependencies: 209
-- Name: Perfil_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "Perfil_id_seq" OWNED BY "Perfil".id;


--
-- TOC entry 196 (class 1259 OID 16472)
-- Name: Preguntas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "Preguntas" (
    id integer NOT NULL,
    id_prueba integer NOT NULL,
    tx_descripcion character varying(100) NOT NULL,
    tx_eje character varying(100) NOT NULL,
    tx_habilidad character varying(100) NOT NULL,
    tx_respuesta_correcta character varying(10) NOT NULL,
    tx_estado character varying(1) NOT NULL,
    id_usuario integer NOT NULL,
    fc_modificacion timestamp without time zone,
    nr_nro_pregunta integer DEFAULT 1 NOT NULL
);


ALTER TABLE "Preguntas" OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 16470)
-- Name: Preguntas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "Preguntas_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Preguntas_id_seq" OWNER TO postgres;

--
-- TOC entry 2298 (class 0 OID 0)
-- Dependencies: 195
-- Name: Preguntas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "Preguntas_id_seq" OWNED BY "Preguntas".id;


--
-- TOC entry 194 (class 1259 OID 16464)
-- Name: Pruebas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "Pruebas" (
    id integer NOT NULL,
    tx_periodo character varying(10) NOT NULL,
    tx_descripcion character varying(100) NOT NULL,
    tx_unidad character varying(100) NOT NULL,
    tx_estado character varying(1) NOT NULL,
    id_usuario integer NOT NULL,
    fc_modificacion timestamp without time zone
);


ALTER TABLE "Pruebas" OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 16462)
-- Name: Pruebas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "Pruebas_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Pruebas_id_seq" OWNER TO postgres;

--
-- TOC entry 2299 (class 0 OID 0)
-- Dependencies: 193
-- Name: Pruebas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "Pruebas_id_seq" OWNED BY "Pruebas".id;


--
-- TOC entry 208 (class 1259 OID 16574)
-- Name: Resultados; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "Resultados" (
    id integer NOT NULL,
    id_prueba_alumno integer NOT NULL,
    id_pregunta integer NOT NULL,
    tx_respuesta character varying(10) NOT NULL,
    tx_estado character varying(1) NOT NULL,
    id_usuario integer NOT NULL,
    fc_modificacion timestamp without time zone
);


ALTER TABLE "Resultados" OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 16572)
-- Name: Resultados_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "Resultados_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Resultados_id_seq" OWNER TO postgres;

--
-- TOC entry 2300 (class 0 OID 0)
-- Dependencies: 207
-- Name: Resultados_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "Resultados_id_seq" OWNED BY "Resultados".id;


--
-- TOC entry 212 (class 1259 OID 16602)
-- Name: Usuarios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE "Usuarios" (
    id integer NOT NULL,
    nr_rut integer NOT NULL,
    tx_dv character varying(1) NOT NULL,
    tx_username character varying(100) NOT NULL,
    tx_userpass character varying(100) NOT NULL,
    tx_usermail character varying(100) NOT NULL,
    id_perfil integer NOT NULL,
    tx_estado character varying(1) NOT NULL,
    id_usuario integer NOT NULL,
    fc_modificacion timestamp without time zone
);


ALTER TABLE "Usuarios" OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16600)
-- Name: Usuarios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "Usuarios_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "Usuarios_id_seq" OWNER TO postgres;

--
-- TOC entry 2301 (class 0 OID 0)
-- Dependencies: 211
-- Name: Usuarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "Usuarios_id_seq" OWNED BY "Usuarios".id;


--
-- TOC entry 2089 (class 2604 OID 16522)
-- Name: Alumno_Curso id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Alumno_Curso" ALTER COLUMN id SET DEFAULT nextval('"Alumno_Curso_id_seq"'::regclass);


--
-- TOC entry 2090 (class 2604 OID 16540)
-- Name: Alumno_Prueba id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Alumno_Prueba" ALTER COLUMN id SET DEFAULT nextval('"Alumno_Prueba_id_seq"'::regclass);


--
-- TOC entry 2083 (class 2604 OID 16459)
-- Name: Alumnos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Alumnos" ALTER COLUMN id SET DEFAULT nextval('"Alumnos_id_seq"'::regclass);


--
-- TOC entry 2081 (class 2604 OID 16443)
-- Name: Asignatura id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Asignatura" ALTER COLUMN id SET DEFAULT nextval('"Asignatura_id_seq"'::regclass);


--
-- TOC entry 2088 (class 2604 OID 16504)
-- Name: Asignatura_Curso id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Asignatura_Curso" ALTER COLUMN id SET DEFAULT nextval('"Asignatura_Curso_id_seq"'::regclass);


--
-- TOC entry 2087 (class 2604 OID 16488)
-- Name: Asignatura_Docente id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Asignatura_Docente" ALTER COLUMN id SET DEFAULT nextval('"Asignatura_docente_id_seq"'::regclass);


--
-- TOC entry 2091 (class 2604 OID 16559)
-- Name: Asignatura_Prueba id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Asignatura_Prueba" ALTER COLUMN id SET DEFAULT nextval('"Asignatura_Prueba_id_seq"'::regclass);


--
-- TOC entry 2082 (class 2604 OID 16451)
-- Name: Cursos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Cursos" ALTER COLUMN id SET DEFAULT nextval('"Cursos_id_seq"'::regclass);


--
-- TOC entry 2080 (class 2604 OID 16432)
-- Name: Docentes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Docentes" ALTER COLUMN id SET DEFAULT nextval('"Docentes_id_seq"'::regclass);


--
-- TOC entry 2093 (class 2604 OID 16595)
-- Name: Perfil id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Perfil" ALTER COLUMN id SET DEFAULT nextval('"Perfil_id_seq"'::regclass);


--
-- TOC entry 2085 (class 2604 OID 16475)
-- Name: Preguntas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Preguntas" ALTER COLUMN id SET DEFAULT nextval('"Preguntas_id_seq"'::regclass);


--
-- TOC entry 2084 (class 2604 OID 16467)
-- Name: Pruebas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Pruebas" ALTER COLUMN id SET DEFAULT nextval('"Pruebas_id_seq"'::regclass);


--
-- TOC entry 2092 (class 2604 OID 16577)
-- Name: Resultados id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Resultados" ALTER COLUMN id SET DEFAULT nextval('"Resultados_id_seq"'::regclass);


--
-- TOC entry 2094 (class 2604 OID 16605)
-- Name: Usuarios id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Usuarios" ALTER COLUMN id SET DEFAULT nextval('"Usuarios_id_seq"'::regclass);


--
-- TOC entry 2269 (class 0 OID 16519)
-- Dependencies: 202
-- Data for Name: Alumno_Curso; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2302 (class 0 OID 0)
-- Dependencies: 201
-- Name: Alumno_Curso_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"Alumno_Curso_id_seq"', 1, false);


--
-- TOC entry 2271 (class 0 OID 16537)
-- Dependencies: 204
-- Data for Name: Alumno_Prueba; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2303 (class 0 OID 0)
-- Dependencies: 203
-- Name: Alumno_Prueba_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"Alumno_Prueba_id_seq"', 1, false);


--
-- TOC entry 2259 (class 0 OID 16456)
-- Dependencies: 192
-- Data for Name: Alumnos; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "Alumnos" (id, nr_rut, tx_dv, tx_nombre, tx_apellido_paterno, tx_apellido_materno, tx_estado, id_usuario, fc_modificacion) VALUES (1, 1, '9', 'tuto', 'tuto', 'tuto', 'A', 1, '2017-04-09 21:41:11');
INSERT INTO "Alumnos" (id, nr_rut, tx_dv, tx_nombre, tx_apellido_paterno, tx_apellido_materno, tx_estado, id_usuario, fc_modificacion) VALUES (2, 1, '9', 'tuto', 'tuto', 'tuto', 'A', 1, '2017-04-09 21:59:45');


--
-- TOC entry 2304 (class 0 OID 0)
-- Dependencies: 191
-- Name: Alumnos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"Alumnos_id_seq"', 2, true);


--
-- TOC entry 2255 (class 0 OID 16440)
-- Dependencies: 188
-- Data for Name: Asignatura; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2267 (class 0 OID 16501)
-- Dependencies: 200
-- Data for Name: Asignatura_Curso; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2305 (class 0 OID 0)
-- Dependencies: 199
-- Name: Asignatura_Curso_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"Asignatura_Curso_id_seq"', 1, false);


--
-- TOC entry 2265 (class 0 OID 16485)
-- Dependencies: 198
-- Data for Name: Asignatura_Docente; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2273 (class 0 OID 16556)
-- Dependencies: 206
-- Data for Name: Asignatura_Prueba; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2306 (class 0 OID 0)
-- Dependencies: 205
-- Name: Asignatura_Prueba_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"Asignatura_Prueba_id_seq"', 1, false);


--
-- TOC entry 2307 (class 0 OID 0)
-- Dependencies: 197
-- Name: Asignatura_docente_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"Asignatura_docente_id_seq"', 1, false);


--
-- TOC entry 2308 (class 0 OID 0)
-- Dependencies: 187
-- Name: Asignatura_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"Asignatura_id_seq"', 1, false);


--
-- TOC entry 2257 (class 0 OID 16448)
-- Dependencies: 190
-- Data for Name: Cursos; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2309 (class 0 OID 0)
-- Dependencies: 189
-- Name: Cursos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"Cursos_id_seq"', 1, false);


--
-- TOC entry 2253 (class 0 OID 16429)
-- Dependencies: 186
-- Data for Name: Docentes; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2310 (class 0 OID 0)
-- Dependencies: 185
-- Name: Docentes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"Docentes_id_seq"', 1, false);


--
-- TOC entry 2277 (class 0 OID 16592)
-- Dependencies: 210
-- Data for Name: Perfil; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2311 (class 0 OID 0)
-- Dependencies: 209
-- Name: Perfil_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"Perfil_id_seq"', 1, false);


--
-- TOC entry 2263 (class 0 OID 16472)
-- Dependencies: 196
-- Data for Name: Preguntas; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "Preguntas" (id, id_prueba, tx_descripcion, tx_eje, tx_habilidad, tx_respuesta_correcta, tx_estado, id_usuario, fc_modificacion, nr_nro_pregunta) VALUES (1, 5, 'p1', 'p1', 'p1', 'p1', 'A', 1, '2017-04-14 23:38:29', 1);
INSERT INTO "Preguntas" (id, id_prueba, tx_descripcion, tx_eje, tx_habilidad, tx_respuesta_correcta, tx_estado, id_usuario, fc_modificacion, nr_nro_pregunta) VALUES (2, 5, 'p2', 'p2', 'p2', 'p2', 'A', 1, '2017-04-14 23:38:29', 2);
INSERT INTO "Preguntas" (id, id_prueba, tx_descripcion, tx_eje, tx_habilidad, tx_respuesta_correcta, tx_estado, id_usuario, fc_modificacion, nr_nro_pregunta) VALUES (3, 6, 'preugunta', 'eje', 'habi', 'A', 'A', 1, '2017-04-16 19:58:03', 1);
INSERT INTO "Preguntas" (id, id_prueba, tx_descripcion, tx_eje, tx_habilidad, tx_respuesta_correcta, tx_estado, id_usuario, fc_modificacion, nr_nro_pregunta) VALUES (4, 6, 'pregunta2', 'eje', 'ha', 'B', 'A', 1, '2017-04-16 19:58:03', 2);


--
-- TOC entry 2312 (class 0 OID 0)
-- Dependencies: 195
-- Name: Preguntas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"Preguntas_id_seq"', 4, true);


--
-- TOC entry 2261 (class 0 OID 16464)
-- Dependencies: 194
-- Data for Name: Pruebas; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO "Pruebas" (id, tx_periodo, tx_descripcion, tx_unidad, tx_estado, id_usuario, fc_modificacion) VALUES (5, '2017', 'Prueba de Ingreso', 'unidad', 'A', 1, '2017-04-14 23:38:29');
INSERT INTO "Pruebas" (id, tx_periodo, tx_descripcion, tx_unidad, tx_estado, id_usuario, fc_modificacion) VALUES (6, '2017', 'prueba prueba', 'hola hola', 'A', 1, '2017-04-16 19:58:03');


--
-- TOC entry 2313 (class 0 OID 0)
-- Dependencies: 193
-- Name: Pruebas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"Pruebas_id_seq"', 6, true);


--
-- TOC entry 2275 (class 0 OID 16574)
-- Dependencies: 208
-- Data for Name: Resultados; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2314 (class 0 OID 0)
-- Dependencies: 207
-- Name: Resultados_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"Resultados_id_seq"', 1, false);


--
-- TOC entry 2279 (class 0 OID 16602)
-- Dependencies: 212
-- Data for Name: Usuarios; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2315 (class 0 OID 0)
-- Dependencies: 211
-- Name: Usuarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"Usuarios_id_seq"', 1, false);


--
-- TOC entry 2110 (class 2606 OID 16524)
-- Name: Alumno_Curso Alumno_Curso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Alumno_Curso"
    ADD CONSTRAINT "Alumno_Curso_pkey" PRIMARY KEY (id);


--
-- TOC entry 2112 (class 2606 OID 16542)
-- Name: Alumno_Prueba Alumno_Prueba_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Alumno_Prueba"
    ADD CONSTRAINT "Alumno_Prueba_pkey" PRIMARY KEY (id);


--
-- TOC entry 2102 (class 2606 OID 16461)
-- Name: Alumnos Alumnos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Alumnos"
    ADD CONSTRAINT "Alumnos_pkey" PRIMARY KEY (id);


--
-- TOC entry 2108 (class 2606 OID 16506)
-- Name: Asignatura_Curso Asignatura_Curso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Asignatura_Curso"
    ADD CONSTRAINT "Asignatura_Curso_pkey" PRIMARY KEY (id);


--
-- TOC entry 2114 (class 2606 OID 16561)
-- Name: Asignatura_Prueba Asignatura_Prueba_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Asignatura_Prueba"
    ADD CONSTRAINT "Asignatura_Prueba_pkey" PRIMARY KEY (id);


--
-- TOC entry 2098 (class 2606 OID 16445)
-- Name: Asignatura Asignatura_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Asignatura"
    ADD CONSTRAINT "Asignatura_pkey" PRIMARY KEY (id);


--
-- TOC entry 2100 (class 2606 OID 16453)
-- Name: Cursos Cursos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Cursos"
    ADD CONSTRAINT "Cursos_pkey" PRIMARY KEY (id);


--
-- TOC entry 2096 (class 2606 OID 16437)
-- Name: Docentes Docentes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Docentes"
    ADD CONSTRAINT "Docentes_pkey" PRIMARY KEY (id);


--
-- TOC entry 2118 (class 2606 OID 16597)
-- Name: Perfil Perfil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Perfil"
    ADD CONSTRAINT "Perfil_pkey" PRIMARY KEY (id);


--
-- TOC entry 2106 (class 2606 OID 16477)
-- Name: Preguntas Preguntas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Preguntas"
    ADD CONSTRAINT "Preguntas_pkey" PRIMARY KEY (id);


--
-- TOC entry 2104 (class 2606 OID 16469)
-- Name: Pruebas Pruebas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Pruebas"
    ADD CONSTRAINT "Pruebas_pkey" PRIMARY KEY (id);


--
-- TOC entry 2116 (class 2606 OID 16579)
-- Name: Resultados Resultados_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Resultados"
    ADD CONSTRAINT "Resultados_pkey" PRIMARY KEY (id);


--
-- TOC entry 2120 (class 2606 OID 16607)
-- Name: Usuarios Usuarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Usuarios"
    ADD CONSTRAINT "Usuarios_pkey" PRIMARY KEY (id);


--
-- TOC entry 2126 (class 2606 OID 16525)
-- Name: Alumno_Curso fk_alumno_curso; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Alumno_Curso"
    ADD CONSTRAINT fk_alumno_curso FOREIGN KEY (id_alumno) REFERENCES "Alumnos"(id);


--
-- TOC entry 2128 (class 2606 OID 16543)
-- Name: Alumno_Prueba fk_alumno_prueba; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Alumno_Prueba"
    ADD CONSTRAINT fk_alumno_prueba FOREIGN KEY (id_alumno) REFERENCES "Alumnos"(id);


--
-- TOC entry 2132 (class 2606 OID 16580)
-- Name: Resultados fk_alumno_prueba; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Resultados"
    ADD CONSTRAINT fk_alumno_prueba FOREIGN KEY (id_prueba_alumno) REFERENCES "Alumno_Prueba"(id);


--
-- TOC entry 2124 (class 2606 OID 16507)
-- Name: Asignatura_Curso fk_asigantura_curso; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Asignatura_Curso"
    ADD CONSTRAINT fk_asigantura_curso FOREIGN KEY (id_asignatura) REFERENCES "Asignatura"(id);


--
-- TOC entry 2130 (class 2606 OID 16562)
-- Name: Asignatura_Prueba fk_asignatura_prueba; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Asignatura_Prueba"
    ADD CONSTRAINT fk_asignatura_prueba FOREIGN KEY (id_asignatura) REFERENCES "Asignatura"(id);


--
-- TOC entry 2122 (class 2606 OID 16489)
-- Name: Asignatura_Docente fk_asignaturas_docentes; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Asignatura_Docente"
    ADD CONSTRAINT fk_asignaturas_docentes FOREIGN KEY (id_docente) REFERENCES "Docentes"(id);


--
-- TOC entry 2127 (class 2606 OID 16530)
-- Name: Alumno_Curso fk_curso_alumno; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Alumno_Curso"
    ADD CONSTRAINT fk_curso_alumno FOREIGN KEY (id_curso) REFERENCES "Cursos"(id);


--
-- TOC entry 2125 (class 2606 OID 16512)
-- Name: Asignatura_Curso fk_curso_asignatura; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Asignatura_Curso"
    ADD CONSTRAINT fk_curso_asignatura FOREIGN KEY (id_curso) REFERENCES "Cursos"(id);


--
-- TOC entry 2123 (class 2606 OID 16494)
-- Name: Asignatura_Docente fk_docentes_asignaturas; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Asignatura_Docente"
    ADD CONSTRAINT fk_docentes_asignaturas FOREIGN KEY (id_asignatura) REFERENCES "Asignatura"(id);


--
-- TOC entry 2129 (class 2606 OID 16548)
-- Name: Alumno_Prueba fk_prueba_alumno; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Alumno_Prueba"
    ADD CONSTRAINT fk_prueba_alumno FOREIGN KEY (id_prueba) REFERENCES "Pruebas"(id);


--
-- TOC entry 2131 (class 2606 OID 16567)
-- Name: Asignatura_Prueba fk_prueba_asignatura; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Asignatura_Prueba"
    ADD CONSTRAINT fk_prueba_asignatura FOREIGN KEY (id_prueba) REFERENCES "Pruebas"(id);


--
-- TOC entry 2121 (class 2606 OID 16478)
-- Name: Preguntas fk_respuesta_prueba; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Preguntas"
    ADD CONSTRAINT fk_respuesta_prueba FOREIGN KEY (id_prueba) REFERENCES "Pruebas"(id);


--
-- TOC entry 2133 (class 2606 OID 16585)
-- Name: Resultados fk_resultado_pregunta; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Resultados"
    ADD CONSTRAINT fk_resultado_pregunta FOREIGN KEY (id_pregunta) REFERENCES "Preguntas"(id);


--
-- TOC entry 2134 (class 2606 OID 16608)
-- Name: Usuarios fk_usuario_perfil; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY "Usuarios"
    ADD CONSTRAINT fk_usuario_perfil FOREIGN KEY (id_perfil) REFERENCES "Perfil"(id);


-- Completed on 2017-04-19 16:20:33

--
-- PostgreSQL database dump complete
--

