﻿using DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IServices
{
    public interface IDocenteService
    {
        IList<DocenteDTO> GetAll();
        DocenteDTO GetById(int id);
        bool Create(DocenteDTO product);
        bool Update(DocenteDTO product);
        bool Delete(int id);
    }
}
