﻿using DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IServices
{
    public interface IAlumnoCursoService
    {
        IList<AlumnoCursoDTO> GetAll();
        AlumnoCursoDTO GetById(int id);
        bool Create(AlumnoCursoDTO product);
        bool Update(AlumnoCursoDTO product);
        bool Delete(int id);
        int GetIdAlumnoCurso(int alumno);
        AlumnoCursoDTO GetAlumnoCursoByIds(int alumno, int year);
    }
}
