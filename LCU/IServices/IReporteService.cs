﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTOS;

namespace IServices
{
    public interface IReporteService
    {
        List<ResultadoAlumnoDTO> getResultadoByAlumno(int periodo, int curso, int prueba);
        List<ResultadoAlumnoDTO> getResultadoByAlumnoEje(int periodo, int curso, int prueba);
        List<ResultadoEjePruebaDTO> getResultadoByEjePrueba(int periodo, int curso, int prueba);
    }
}
