﻿using DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IServices
{
    public interface IPreguntaService
    {
        IList<PreguntaDTO> GetAll();
        IList<PreguntaDTO> GetPreguntasByPrueba(int prueba);
        PreguntaDTO GetById(int id);
        bool Create(PreguntaDTO product);
        bool Create(IList<PreguntaDTO> product);
        bool Update(PreguntaDTO product);
        bool Update(IList<PreguntaDTO> product);
        bool Delete(int id);
    }
}
