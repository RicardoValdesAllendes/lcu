﻿using DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IServices
{
    public interface IPruebaService
    {
        IList<PruebaDTO> GetAll();
        PruebaDTO GetById(int id);
        bool Create(PruebaDTO product);
        bool Update(PruebaDTO product);
        bool Delete(int id);
    }
}
