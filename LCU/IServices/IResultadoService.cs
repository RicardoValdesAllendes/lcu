﻿using DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IServices
{
    public interface IResultadoService
    {
        IList<ResultadoDTO> GetAll();
        ResultadoDTO GetById(int id);
        bool Create(ResultadoDTO product);
        bool Create(IList<ResultadoDTO> product);
        bool Update(ResultadoDTO product);
        bool Delete(int id);
        ResultadoDTO getRespuestaByPreguntaAlumnoPrueba(int pregunta, int alumno, int prueba);
    }
}
