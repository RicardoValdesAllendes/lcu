﻿using DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IServices
{
    public interface ICursoService
    {
        IList<CursoDTO> GetAll();
        CursoDTO GetById(int id);
        bool Create(CursoDTO product);
        bool Update(CursoDTO product);
        bool Delete(int id);
    }
}
