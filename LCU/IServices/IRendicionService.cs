﻿using DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IServices
{
    public interface IRendicionService
    {
        IList<AlumnoPruebaDTO> GetAll();
        AlumnoPruebaDTO GetById(int id);
        bool Create(AlumnoPruebaDTO product);
        bool Update(AlumnoPruebaDTO product);
        bool Delete(int id);
        AlumnoPruebaDTO getByAlumnoPruebaIds(int alumno, int prueba);
    }
}
