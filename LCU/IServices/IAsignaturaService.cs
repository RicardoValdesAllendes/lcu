﻿using DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IServices
{
    public interface IAsignaturaService
    {
        IList<AsignaturaDTO> GetAll();
        AsignaturaDTO GetById(int id);
        bool Create(AsignaturaDTO product);
        bool Update(AsignaturaDTO product);
        bool Delete(int id);
    }
}
