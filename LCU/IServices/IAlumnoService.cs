﻿using DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IServices
{
    public interface IAlumnoService
    {
        IList<AlumnoDTO> GetAll();
        IList<AlumnoDTO> GetAlumnosbyCurso(int id);
        AlumnoDTO GetById(int id);
        bool Create(AlumnoDTO product);
        bool Update(AlumnoDTO product);
        bool Delete(int id);
    }
}
