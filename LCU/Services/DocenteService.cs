﻿using DTOS;
using IServices;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class DocenteService : IDocenteService
    {
        IRepository<DocenteDTO> _docenteRepository;
        public DocenteService(IRepository<DocenteDTO> docenteRepository)
        {
            _docenteRepository = docenteRepository;
        }
        public bool Create(DocenteDTO product)
        {
            return _docenteRepository.Create(product);
        }

        public bool Delete(int id)
        {
            return _docenteRepository.Delete(GetById(id));
        }

        public IList<DocenteDTO> GetAll()
        {
            return _docenteRepository.GetAll().ToList();
        }

        public DocenteDTO GetById(int id)
        {
            return _docenteRepository.GetById(id);
        }

        public bool Update(DocenteDTO product)
        {
            return _docenteRepository.Update(product);
        }
    }
}
