﻿using DTOS;
using IServices;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ResultadoService:IResultadoService
    {
        private IRepository<ResultadoDTO> _resultadoRepository;

        public ResultadoService(IRepository<ResultadoDTO> resultadoRepository)
        {
            _resultadoRepository = resultadoRepository;
        }

        public bool Create(ResultadoDTO product)
        {
            return _resultadoRepository.Create(product);
        }

        public bool Create(IList<ResultadoDTO> product)
        {
            return _resultadoRepository.Create(product);
        }

        public bool Delete(int id)
        {
            return _resultadoRepository.Delete(GetById(id));
        }

        public IList<ResultadoDTO> GetAll()
        {
            return _resultadoRepository.GetAll().ToList();
        }

        public ResultadoDTO GetById(int id)
        {
            return _resultadoRepository.GetById(id);
        }

        public ResultadoDTO getRespuestaByPreguntaAlumnoPrueba(int pregunta, int alumno, int prueba)
        {
            Expression<Func<ResultadoDTO, bool>> query = (x) => x.Pregunta.Id == pregunta && x.AlumnoPrueba.Alumno.Id == alumno && x.AlumnoPrueba.Prueba.Id == prueba;
            var respuestaPreguntaAlumno = _resultadoRepository.FilterBy(query);
            return respuestaPreguntaAlumno.ToList().FirstOrDefault();
        }

        public bool Update(ResultadoDTO product)
        {
            return _resultadoRepository.Save(product);
        }
        
    }
}
