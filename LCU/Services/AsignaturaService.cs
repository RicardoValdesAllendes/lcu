﻿using DTOS;
using IServices;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class AsignaturaService : IAsignaturaService
    {
        IRepository<AsignaturaDTO> _asignaturaRepository;

        public AsignaturaService(IRepository<AsignaturaDTO> asignaturaRepository)
        {
            _asignaturaRepository = asignaturaRepository;
        }

        public bool Create(AsignaturaDTO product)
        {
            return _asignaturaRepository.Create(product);
        }

        public bool Delete(int id)
        {
            return _asignaturaRepository.Delete(GetById(id));
        }

        public IList<AsignaturaDTO> GetAll()
        {
            return _asignaturaRepository.GetAll().ToList();
        }

        public AsignaturaDTO GetById(int id)
        {
            return _asignaturaRepository.GetById(id);
        }

        public bool Update(AsignaturaDTO product)
        {
            return _asignaturaRepository.Update(product);
        }
    }
}
