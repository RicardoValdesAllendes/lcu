﻿using DTOS;
using IServices;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class CursoService : ICursoService
    {
        IRepository<CursoDTO> _cursoRepository;

        public CursoService(IRepository<CursoDTO> cursoRepository)
        {
            _cursoRepository = cursoRepository;
        }

        public bool Create(CursoDTO product)
        {
            return _cursoRepository.Create(product);
        }

        public bool Delete(int id)
        {
            return _cursoRepository.Delete(GetById(id));
        }

        public IList<CursoDTO> GetAll()
        {
            return _cursoRepository.GetAll().ToList();
        }

        public CursoDTO GetById(int id)
        {
            return _cursoRepository.GetById(id);
        }

        public bool Update(CursoDTO product)
        {
            return _cursoRepository.Update(product);
        }
    }
}
