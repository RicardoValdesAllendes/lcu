﻿using IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTOS;
using Repository;
using System.Linq.Expressions;

namespace Services
{
    public class PruebaService : IPruebaService
    {
        IRepository<PruebaDTO> _pruebaRepository;
        IRepository<PreguntaDTO> _preguntaRepository;
        public PruebaService(IRepository<PruebaDTO> pruebaRepository, IRepository<PreguntaDTO> preguntaRepository)
        {
            _pruebaRepository = pruebaRepository;
            _preguntaRepository = preguntaRepository;
        }
        public bool Create(PruebaDTO product)
        {
            return _pruebaRepository.Create(product);
        }

        public bool Delete(int id)
        {
            return _pruebaRepository.Delete(GetById(id));
        }

        public IList<PruebaDTO> GetAll()
        {
            return _pruebaRepository.GetAll().ToList();
        }

        public PruebaDTO GetById(int id)
        {
            return _pruebaRepository.GetById(id);
        }

        public bool Update(PruebaDTO product)
        {
            return _pruebaRepository.Update(product);
        }
    }
}
