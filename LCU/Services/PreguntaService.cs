﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTOS;
using Repository;
using IServices;
using System.Linq.Expressions;

namespace Services
{
    public class PreguntaService : IPreguntaService
    {
        IRepository<PreguntaDTO> _preguntaRepository;

        public PreguntaService(IRepository<PreguntaDTO> preguntaRepository)
        {
            _preguntaRepository = preguntaRepository;
        }

        public bool Create(PreguntaDTO product)
        {
            return _preguntaRepository.Create(product);
        }

        public bool Create(IList<PreguntaDTO> product)
        {
            return _preguntaRepository.Create(product);
        }

        public bool Delete(int id)
        {
            return _preguntaRepository.Delete(GetById(id));
        }

        public IList<PreguntaDTO> GetAll()
        {
            return _preguntaRepository.GetAll().ToList();
        }

        public PreguntaDTO GetById(int id)
        {
            return _preguntaRepository.GetById(id);
        }

        public IList<PreguntaDTO> GetPreguntasByPrueba(int prueba)
        {
            Expression<Func<PreguntaDTO, bool>> query = (x) => x.Prueba.Id == prueba;
            var preguntas = _preguntaRepository.FilterBy(query).ToList();
            return preguntas;
        }
        public bool Update(PreguntaDTO product)
        {
            return _preguntaRepository.Update(product);
        }

        public bool Update(IList<PreguntaDTO> product)
        {
            return _preguntaRepository.Save(product);
        }
    }
}
