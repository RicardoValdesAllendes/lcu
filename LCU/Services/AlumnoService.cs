﻿using DTOS;
using IServices;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class AlumnoService : IAlumnoService
    {
        IRepository<AlumnoDTO> _alumnoRepository;
        IRepository<AlumnoCursoDTO> _alumnoCursoRepository;

        public AlumnoService(IRepository<AlumnoDTO> alumnoRepository, IRepository<AlumnoCursoDTO> alumnoCursoRepository)
        {
            _alumnoRepository = alumnoRepository;
            _alumnoCursoRepository = alumnoCursoRepository;
        }

        public bool Create(AlumnoDTO product)
        {
            return _alumnoRepository.Create(product);
        }

        public bool Delete(int id)
        {
            return _alumnoRepository.Delete(GetById(id));
        }

        public IList<AlumnoDTO> GetAll()
        {
            return _alumnoRepository.GetAll().ToList();
        }

        public IList<AlumnoDTO> GetAlumnosbyCurso(int id)
        {
            Expression<Func<AlumnoCursoDTO, bool>> query = (x) => x.Curso.Id == id;
            var alumnoCurso = _alumnoCursoRepository.FilterBy(query).ToList();
            var retorno = new List<AlumnoDTO>();
            foreach (var item in alumnoCurso)
            {
                retorno.Add(GetById(item.Alumno.Id));
            }
            return retorno;
        }

        public AlumnoDTO GetById(int id)
        {
            return _alumnoRepository.GetById(id);
        }

        public bool Update(AlumnoDTO product)
        {
            return _alumnoRepository.Update(product);
        }
    }
}
