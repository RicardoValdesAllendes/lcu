﻿using DTOS;
using IServices;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class AlumnoCursoService : IAlumnoCursoService 
    {
        IRepository<AlumnoCursoDTO> _alumnoCursoRepository;

        public AlumnoCursoService(IRepository<AlumnoCursoDTO> alumnoCursoRepository)
        {
            _alumnoCursoRepository = alumnoCursoRepository;
        }

        public bool Create(AlumnoCursoDTO product)
        {
            return _alumnoCursoRepository.Create(product);
        }

        public bool Delete(int id)
        {
            return _alumnoCursoRepository.Delete(GetById(id));
        }

        public IList<AlumnoCursoDTO> GetAll()
        {
            return _alumnoCursoRepository.GetAll().ToList();
        }

        public AlumnoCursoDTO GetById(int id)
        {
            return _alumnoCursoRepository.GetById(id);
        }

        public bool Update(AlumnoCursoDTO product)
        {
            return _alumnoCursoRepository.Update(product);
        }

       public int  GetIdAlumnoCurso(int alumno)
        {
            Expression<Func<AlumnoCursoDTO, bool>> query = (x) => x.Alumno.Id == alumno; 
            var idAlumnoCurso = _alumnoCursoRepository.FilterBy(query).ToList(); ;
            return idAlumnoCurso[0].Curso.Id  ;
    }

        public AlumnoCursoDTO GetAlumnoCursoByIds(int alumno, int year)
        {
            Expression<Func<AlumnoCursoDTO, bool>> query = (x) => x.Alumno.Id == alumno && x.Año == year;
            var idAlumnoCurso = _alumnoCursoRepository.FilterBy(query).ToList();
            return idAlumnoCurso[0];
        }
    }
}
