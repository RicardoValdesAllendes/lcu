﻿using IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTOS;
using Repository;
using System.Linq.Expressions;

namespace Services
{
    public class RendicionService : IRendicionService
    {
        IRepository<AlumnoPruebaDTO> _alumnoPruebaRepository;
        public RendicionService(IRepository<AlumnoPruebaDTO> alumnoPruebaRepository)
        {
            _alumnoPruebaRepository = alumnoPruebaRepository;
        }


        public bool Create(AlumnoPruebaDTO product)
        {
            return _alumnoPruebaRepository.Create(product);
        }

        public bool Delete(int id)
        {
            return _alumnoPruebaRepository.Delete(GetById(id));
        }

        public IList<AlumnoPruebaDTO> GetAll()
        {
            return _alumnoPruebaRepository.GetAll().ToList();
        }

        public AlumnoPruebaDTO getByAlumnoPruebaIds(int alumno, int prueba)
        {
            Expression<Func<AlumnoPruebaDTO, bool>> query = (x) => x.Alumno.Id == alumno && x.Prueba.Id == prueba;
            var alumnoPrueba = _alumnoPruebaRepository.FilterBy(query);
            return alumnoPrueba.ToList().FirstOrDefault();
        }

        public AlumnoPruebaDTO GetById(int id)
        {
            return _alumnoPruebaRepository.GetById(id);
        }

        public bool Update(AlumnoPruebaDTO product)
        {
            return _alumnoPruebaRepository.Update(product);
        }
    }
}
