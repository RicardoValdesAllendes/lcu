﻿using DTOS;
using IServices;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class PerfilService : IPerfilService
    {
        private IRepository<PerfilDTO> _perfilRepository;

        public PerfilService(IRepository<PerfilDTO> perfilRepository)
        {
            _perfilRepository = perfilRepository;
        }

        public bool Create(PerfilDTO product)
        {
            return _perfilRepository.Create(product);
        }

        public bool Delete(int id)
        {
            return _perfilRepository.Delete(GetById(id));
        }

        public IList<PerfilDTO> GetAll()
        {
            return _perfilRepository.GetAll().ToList();
        }

        public PerfilDTO GetById(int id)
        {
            return _perfilRepository.GetById(id);
        }

        public bool Update(PerfilDTO product)
        {
            return _perfilRepository.Update(product);
        }
    }
}
