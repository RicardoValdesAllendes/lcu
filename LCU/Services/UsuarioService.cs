﻿using DTOS;
using IServices;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class UsuarioService : IUsuarioService
    {
        private IRepository<UsuarioDTO> _usuarioRepository;

        public UsuarioService(IRepository<UsuarioDTO> usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }


        public bool Create(UsuarioDTO product)
        {
            return _usuarioRepository.Create(product);
        }

        public IList<UsuarioDTO> GetAll()
        {
            return _usuarioRepository.GetAll().ToList();
        }

        public UsuarioDTO GetById(int id)
        {
            return _usuarioRepository.GetById(id);
        }

        public bool Update(UsuarioDTO product)
        {
            return _usuarioRepository.Update(product);
        }

        public UsuarioDTO UserLogin(string userName, string password)
        {
            Expression<Func<UsuarioDTO, bool>> query = (x) => x.UserName.ToUpper() == userName.ToUpper() && x.UserPass == password;
            var user = _usuarioRepository.FilterBy(query);
            return user.ToList().FirstOrDefault();
        }
    }
}
