﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTOS;
using IServices;
using Repository;
using System.Linq.Expressions;

namespace Services
{
    public class ReporteService : IReporteService
    {
        IRepository<AlumnoCursoDTO> _alumnoRepository;
        IRepository<ResultadoDTO> _resultadoRepository;
        IRepository<PruebaDTO> _pruebaRepository;
        IRepository<PreguntaDTO> _preguntaRepository;

        public ReporteService(IRepository<AlumnoCursoDTO> alumnoRepository,
        IRepository<ResultadoDTO> resultadoRepository, IRepository<PruebaDTO> pruebaRepository, IRepository<PreguntaDTO> preguntaRepository)
        {
            _alumnoRepository = alumnoRepository;
            _resultadoRepository = resultadoRepository;
            _pruebaRepository = pruebaRepository;
            _preguntaRepository = preguntaRepository;
        }

        public List<ResultadoAlumnoDTO> getResultadoByAlumno(int periodo, int curso, int prueba)
        {
            Expression<Func<AlumnoCursoDTO, bool>> query = (x) => x.Año == periodo && x.Curso.Id == curso;
            var alumnoCurso = _alumnoRepository.FilterBy(query);
            List<ResultadoAlumnoDTO> salida = new List<ResultadoAlumnoDTO>();

            foreach (var alumno in alumnoCurso)
            {
                ResultadoAlumnoDTO res = new ResultadoAlumnoDTO();
                res.Curso = alumno.Curso.Descripcion;
                res.Rut = alumno.Alumno.Rut + "-" + alumno.Alumno.Dv;
                res.Nombres = alumno.Alumno.Nombre;
                res.Apellidos = alumno.Alumno.ApellidoPaterno + " " + alumno.Alumno.ApellidoMaterno;

                Expression<Func<ResultadoDTO, bool>> iQuery = (x) => x.AlumnoPrueba.Prueba.Id == prueba && x.AlumnoPrueba.Alumno.Id == alumno.Alumno.Id;
                var pAlumno = _resultadoRepository.FilterBy(iQuery);
                if (pAlumno.ToList().Count > 0)
                {
                    foreach (var item in pAlumno)
                    {
                        if (item.Respuesta != null)
                        {
                            res.Correctas += item.Respuesta.Equals(item.Pregunta.RespuestaCorrecta.ToUpper()) ? 1 : 0;
                            res.Omitidas += (item.Pregunta.RespuestaCorrecta == "") ? 1 : 0;
                        }
                        else
                        {
                            res.Omitidas += 1;
                        }
                    }

                    res.Puntaje = pAlumno.ToList()[0].AlumnoPrueba.Puntaje;
                    res.Nota = pAlumno.ToList()[0].AlumnoPrueba.Nota;
                    res.Nivel = Nivel(pAlumno.ToList()[0].AlumnoPrueba.Nota);
                }
                salida.Add(res);
            }
            return salida;
        }

        private string Nivel(int nota)
        {
            string retorno = "";
            if (nota <= 39)
            {
                retorno = "Bajo";
            }
            else if (nota <= 50 && nota >= 40)
            {
                retorno = "Medio";
            }
            else if (nota > 50 && nota < 70)
            {
                retorno = "Medio Alto";
            }
            else
            {
                retorno = "Alto";
            }
            return retorno;
        }

        public List<ResultadoAlumnoDTO> getResultadoByAlumnoEje(int periodo, int curso, int prueba)
        {
            Expression<Func<AlumnoCursoDTO, bool>> query = (x) => x.Año == periodo && x.Curso.Id == curso;
            var alumnoCurso = _alumnoRepository.FilterBy(query);
            List<ResultadoAlumnoDTO> salida = new List<ResultadoAlumnoDTO>();

            foreach (var alumno in alumnoCurso)
            {
                ResultadoAlumnoDTO res = new ResultadoAlumnoDTO();
                res.Curso = alumno.Curso.Descripcion;
                res.Rut = alumno.Alumno.Rut + "-" + alumno.Alumno.Dv;
                res.Nombres = alumno.Alumno.Nombre;
                res.Apellidos = alumno.Alumno.ApellidoPaterno + " " + alumno.Alumno.ApellidoMaterno;

                Expression<Func<ResultadoDTO, bool>> iQuery = (x) => x.AlumnoPrueba.Prueba.Id == prueba && x.AlumnoPrueba.Alumno.Id == alumno.Alumno.Id;
                var pAlumno = _resultadoRepository.FilterBy(iQuery);

                // 1-3 Bajo 4-5 Medio 5-7 Medio Alto
                if (pAlumno.ToList().Count > 0)
                {
                    foreach (var item in pAlumno)
                    {
                        if (item.Respuesta != null)
                        {
                            res.Correctas += item.Respuesta.Equals(item.Pregunta.RespuestaCorrecta.ToUpper()) ? 1 : 0;
                            res.Omitidas += (item.Pregunta.RespuestaCorrecta == "") ? 1 : 0;
                        }
                        else
                        {
                            res.Omitidas += 1;
                        }
                        res.Eje = item.Pregunta.Eje;

                    }
                    var gen = pAlumno.ToList()[0];
                    res.Asignatura = gen.AlumnoPrueba.Prueba.Unidad;
                    res.Prueba = gen.AlumnoPrueba.Prueba.Descripcion;
                    res.Puntaje = gen.AlumnoPrueba.Puntaje;
                    res.Nivel = Nivel(gen.AlumnoPrueba.Nota);
                }
                salida.Add(res);

            }
            return salida;
        }

        public List<ResultadoEjePruebaDTO> getResultadoByEjePrueba(int periodo, int curso, int prueba)
        {
            Expression<Func<AlumnoCursoDTO, bool>> query = (x) => x.Año == periodo && x.Curso.Id == curso;
            var alumnoCurso = _alumnoRepository.FilterBy(query);
            Expression<Func<PreguntaDTO, bool>> queryPrueba = (x) => x.Prueba.Id == prueba;
            var ejes = (_preguntaRepository.FilterBy(queryPrueba).ToList()).Select(x => x.Eje).Distinct();
            List<ResultadoEjePruebaDTO> salida = new List<ResultadoEjePruebaDTO>();

            foreach (var eje in ejes)
            {
                ResultadoEjePruebaDTO res = new ResultadoEjePruebaDTO();

                int Correctas = 0;
                int Omitidas = 0;
                
                res.EjeHabilidad = eje;
                res.NroAlumnas = alumnoCurso.Count();
                foreach (var alumno in alumnoCurso)
                {
                    int cont = 0;
                    Expression<Func<ResultadoDTO, bool>> iQuery = (x) => x.AlumnoPrueba.Prueba.Id == prueba && x.AlumnoPrueba.Alumno.Id == alumno.Alumno.Id;
                    var pAlumno = _resultadoRepository.FilterBy(iQuery);
                    if (pAlumno.ToList().Count > 0)
                    {
                        foreach (var item in pAlumno)
                        {
                            if (item.Pregunta.Eje == eje)
                            {
                                if (item.Respuesta != null)
                                {
                                    Correctas += item.Respuesta.Equals(item.Pregunta.RespuestaCorrecta.ToUpper()) ? 1 : 0;
                                    Omitidas += (item.Pregunta.RespuestaCorrecta == "") ? 1 : 0;
                                }
                                else
                                {
                                    Omitidas += 1;
                                }
                                cont++;
                            }
                        }
                    }
                    string nivel = CalculaNivelEje(cont, Correctas);
                    switch (nivel)
                    {
                        case "Bajo":
                            res.NivelBajo += 1;
                            break;
                        case "MedioBajo":
                            res.NivelMedioBajo += 1;
                            break;
                        case "MedioAlto":
                            res.NivelMedioAlto += 1;
                            break;
                        case "Alto":
                            res.NivelAlto += 1;
                            break;
                    }

                }
                salida.Add(res);
            }

            return salida;
        }

        private string CalculaNivelEje(int totalEje, int correctas)
        {
            string retorno = "";
            int porcentaje ;
            if (totalEje == 0)
            {
                porcentaje = 0;
            }
            else
            {
                porcentaje = ((correctas * 100) / totalEje);
            }
            
            if (porcentaje <= 50)
            {
                retorno = "Bajo";
            }
            else if (porcentaje > 50 && porcentaje <= 70)
            {
                retorno = "MedioBajo";
            }
            else if (porcentaje > 70 && porcentaje <= 90)
            {
                retorno = "MedioAlto";
            }
            else
            {
                retorno = "Alto";
            }
            return retorno;
        }
    }
}
