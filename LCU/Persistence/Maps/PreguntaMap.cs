﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTOS;
using FluentNHibernate.Mapping;

namespace Persistence.Maps
{
    public class PreguntaMap : ClassMap<PreguntaDTO>
    {
        public PreguntaMap()
        {
            Schema("\"public\"");
            Table("\"Preguntas\"");

            Id(x => x.Id).Column("id").GeneratedBy.Identity();
            Map(x => x.Estado).Column("tx_estado");
            Map(x => x.FechaModificacion).Column("fc_modificacion");
            Map(x => x.IdUsuario).Column("id_usuario");

            Map(x => x.NumeroPregunta).Column("nr_nro_pregunta");
            Map(x => x.Descripcion).Column("tx_descripcion");
            Map(x => x.Eje).Column("tx_eje");
            Map(x => x.Habilidad).Column("tx_habilidad");
            Map(x => x.RespuestaCorrecta).Column("tx_respuesta_correcta");

            References(x => x.Prueba).Column("id_prueba").LazyLoad();
        }
    }
}
