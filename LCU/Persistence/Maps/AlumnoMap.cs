﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTOS;
using FluentNHibernate.Mapping;

namespace Persistence.Maps
{
    public class AlumnoMap : ClassMap<AlumnoDTO>
    {
        public AlumnoMap()
        {
            Schema("\"public\"");
            Table("\"Alumnos\"");

            Id(x => x.Id).Column("id").GeneratedBy.Identity();
            Map(x => x.Estado).Column("tx_estado");
            Map(x => x.FechaModificacion).Column("fc_modificacion");
            Map(x => x.IdUsuario).Column("id_usuario");

            Map(x => x.Rut).Column("nr_rut");
            Map(x => x.Dv).Column("tx_dv");
            Map(x => x.Nombre).Column("tx_nombre");
            Map(x => x.ApellidoPaterno).Column("tx_apellido_paterno");
            Map(x => x.ApellidoMaterno).Column("tx_apellido_materno");

        }
    }
}
