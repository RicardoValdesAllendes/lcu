﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTOS;
using FluentNHibernate.Mapping;

namespace Persistence.Maps
{
    public class AsignaturaMap:ClassMap<AsignaturaDTO>
    {
        public AsignaturaMap()
        {
            Schema("\"public\"");
            Table("\"Asignatura\"");

            Id(x => x.Id).Column("id").GeneratedBy.Identity();
            Map(x => x.Estado).Column("tx_estado");
            Map(x => x.FechaModificacion).Column("fc_modificacion");
            Map(x => x.IdUsuario).Column("id_usuario");

            Map(x => x.Nombre).Column("tx_nombre");
        }
    }
}
