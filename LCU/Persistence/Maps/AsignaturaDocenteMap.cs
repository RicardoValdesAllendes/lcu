﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTOS;
using FluentNHibernate.Mapping;

namespace Persistence.Maps
{
    public class AsignaturaDocenteMap:ClassMap<AsignaturaDocenteDTO>
    {
        public AsignaturaDocenteMap()
        {
            Schema("\"public\"");
            Table("\"Asignatura_Docente\"");

            Id(x => x.Id).Column("id").GeneratedBy.Identity();
            Map(x => x.Estado).Column("tx_estado");
            Map(x => x.FechaModificacion).Column("fc_modificacion");
            Map(x => x.IdUsuario).Column("id_usuario");

            References(x => x.Asignatura).Column("id_asignatura").LazyLoad();
            References(x => x.Docente).Column("id_docente").LazyLoad();
        }
    }
}
