﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTOS;
using FluentNHibernate.Mapping;

namespace Persistence.Maps
{
    public class ResultadoMap : ClassMap<ResultadoDTO>
    {
        public ResultadoMap()
        {
            Schema("\"public\"");
            Table("\"Resultados\"");

            Id(x => x.Id).Column("id").GeneratedBy.Identity();
            Map(x => x.Estado).Column("tx_estado");
            Map(x => x.FechaModificacion).Column("fc_modificacion");
            Map(x => x.IdUsuario).Column("id_usuario");

            Map(x => x.Respuesta).Column("tx_respuesta");

            References(x => x.AlumnoPrueba).Column("id_prueba_alumno").LazyLoad();
            References(x => x.Pregunta).Column("id_pregunta").LazyLoad();
        }
    }
}
