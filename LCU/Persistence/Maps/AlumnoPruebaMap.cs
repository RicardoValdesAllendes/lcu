﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTOS;
using FluentNHibernate.Mapping;

namespace Persistence.Maps
{
    public class AlumnoPruebaMap : ClassMap<AlumnoPruebaDTO>
    {
        public AlumnoPruebaMap()
        {
            Schema("\"public\"");
            Table("\"Alumno_Prueba\"");

            Id(x => x.Id).Column("id").GeneratedBy.Identity();
            Map(x => x.Estado).Column("tx_estado");
            Map(x => x.FechaModificacion).Column("fc_modificacion");
            Map(x => x.IdUsuario).Column("id_usuario");

            Map(x => x.Puntaje).Column("nr_puntaje_obtenido");
            Map(x => x.Nota).Column("nr_nota");

            References(x => x.Prueba).Column("id_prueba").LazyLoad();
            References(x => x.Alumno).Column("id_alumno").LazyLoad();
        }
    }
}
