﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTOS;
using FluentNHibernate.Mapping;

namespace Persistence.Maps
{
    public class AlumnoCursoMap : ClassMap<AlumnoCursoDTO>
    {
        public AlumnoCursoMap()
        {
            Schema("\"public\"");
            Table("\"Alumno_Curso\"");

            Id(x => x.Id).Column("id").GeneratedBy.Identity();
            Map(x => x.Estado).Column("tx_estado");
            Map(x => x.FechaModificacion).Column("fc_modificacion");
            Map(x => x.IdUsuario).Column("id_usuario");

            Map(x => x.Año).Column("nr_ano");

            References(x => x.Alumno).Column("id_alumno").LazyLoad();
            References(x => x.Curso).Column("id_curso").LazyLoad();
        }
    }
}
