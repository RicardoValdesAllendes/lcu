﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTOS;
using FluentNHibernate.Mapping;

namespace Persistence.Maps
{
    public class PruebaMap : ClassMap<PruebaDTO>
    {
        public PruebaMap()
        {
            Schema("\"public\"");
            Table("\"Pruebas\"");

            Id(x => x.Id).Column("id").GeneratedBy.Identity();
            Map(x => x.Estado).Column("tx_estado");
            Map(x => x.FechaModificacion).Column("fc_modificacion");
            Map(x => x.IdUsuario).Column("id_usuario");

            Map(x => x.Periodo).Column("tx_periodo");
            Map(x => x.Descripcion).Column("tx_descripcion");
            Map(x => x.Unidad).Column("tx_unidad");
        }
    }
}
