﻿using DTOS;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Maps
{
    public class UsuarioMap : ClassMap<UsuarioDTO>
    {
        public UsuarioMap()
        {
            Schema("\"public\"");
            Table("\"Usuarios\"");

            Id(x => x.Id).Column("id").GeneratedBy.Identity();
            Map(x => x.Estado).Column("tx_estado");
            Map(x => x.FechaModificacion).Column("fc_modificacion");
            Map(x => x.IdUsuario).Column("id_usuario");

            Map(x => x.Rut).Column("nr_rut");
            Map(x => x.Dv).Column("tx_dv");
            Map(x => x.UserName).Column("tx_username");
            Map(x => x.UserPass).Column("tx_userpass");
            Map(x => x.UserMail).Column("tx_usermail");

            References(x => x.Perfil).Column("id_perfil").LazyLoad();

        }
    }
}
