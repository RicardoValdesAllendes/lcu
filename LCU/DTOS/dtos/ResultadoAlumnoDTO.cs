﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOS
{
    public class ResultadoAlumnoDTO
    {
        public string Curso { get; set; }
        public string Rut { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public int Correctas { get; set; }
        public int Omitidas { get; set; }
        public double Puntaje { get; set; }
        public double Nota { get; set; }
        public string Nivel { get; set; }
        public string Asignatura { get; set; }
        public string Eje { get; set; }
        public string Prueba { get; set;  }
    }
}
