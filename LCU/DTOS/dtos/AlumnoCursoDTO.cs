﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOS
{
    public class AlumnoCursoDTO : baseDTO
    {
        public virtual AlumnoDTO Alumno { get; set; }
        public virtual CursoDTO Curso { get; set; }
        public virtual int Año { get; set; }
    }
}
