﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOS
{
    public class DocenteDTO:baseDTO
    {
        public virtual int Rut { get; set; }
        public virtual string Dv { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string ApellidoPaterno { get; set; }
        public virtual string ApellidoMaterno { get; set; }
    }
}
