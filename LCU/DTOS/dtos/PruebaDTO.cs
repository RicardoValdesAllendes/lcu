﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOS
{
    public class PruebaDTO : baseDTO
    {
        public virtual string Periodo { get; set; }
        public virtual string Descripcion { get; set; }
        public virtual string Unidad { get; set; }
    }
}
