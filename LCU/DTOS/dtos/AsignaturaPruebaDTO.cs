﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOS
{
    public class AsignaturaPruebaDTO : baseDTO
    {
        public virtual AsignaturaDTO Asignatura { get; set; }
        public virtual PruebaDTO Prueba { get; set; }
    }
}
