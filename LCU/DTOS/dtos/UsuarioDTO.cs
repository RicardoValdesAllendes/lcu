﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOS
{
    public class UsuarioDTO: baseDTO
    {
        public virtual int Rut { get; set; }
        public virtual int Dv { get; set; }
        public virtual string UserName { get; set; }
        public virtual string UserPass { get; set; }
        public virtual string UserMail { get; set; }
        public virtual PerfilDTO Perfil { get; set; }
    }
}
