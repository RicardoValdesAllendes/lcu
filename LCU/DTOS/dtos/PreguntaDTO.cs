﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOS
{
    public class PreguntaDTO : baseDTO
    {
        public virtual PruebaDTO Prueba { get; set; }
        public virtual int NumeroPregunta { get; set; }
        public virtual string Descripcion { get; set; }
        public virtual string Eje { get; set; }
        public virtual string Habilidad { get; set; }
        public virtual string RespuestaCorrecta { get; set; }
    }
}
