﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOS
{
    public class AlumnoPruebaDTO : baseDTO
    {
        public virtual AlumnoDTO Alumno { get; set; }
        public virtual PruebaDTO Prueba { get; set; }
        public virtual CursoDTO Curso { get; set; }
        public virtual AlumnoCursoDTO AlumnoCurso { get; set; }
    public virtual int Puntaje { get; set; }
    public virtual int Nota { get; set; }
}
}
