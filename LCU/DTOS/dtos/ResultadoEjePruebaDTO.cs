﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOS
{
    public class ResultadoEjePruebaDTO
    {
        public string EjeHabilidad { get; set; }
        public int NroAlumnas { get; set; }
        public int NivelBajo { get; set; }
        public int NivelMedioBajo { get; set; }
        public int NivelMedioAlto { get; set; }
        public int NivelAlto { get; set; }
    }
}
