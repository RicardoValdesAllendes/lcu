﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOS
{
    public class baseDTO
    {
        public virtual int Id { get; set; }
        public virtual string Estado { get; set; }
        public virtual int IdUsuario { get; set; }
        public virtual DateTime FechaModificacion { get; set; }
    }
}
