﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOS
{
    public class AsignaturaCursoDTO : baseDTO
    {
        public virtual AsignaturaDTO Asignatura { get; set; }
        public virtual CursoDTO Curso { get; set; }
    }
}
