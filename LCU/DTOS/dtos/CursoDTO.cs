﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOS
{
    public class CursoDTO : baseDTO
    {
        public virtual string Curso { get; set; }
        public virtual string Descripcion { get; set; }
    }
}
