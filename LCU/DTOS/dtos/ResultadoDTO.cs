﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOS
{
    public class ResultadoDTO : baseDTO
    {
        public virtual AlumnoPruebaDTO AlumnoPrueba { get; set; }
        public virtual PreguntaDTO Pregunta { get; set; }
        public virtual string Respuesta { get; set; }
    }
}
