﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTOS
{
    public class AsignaturaDocenteDTO : baseDTO
    {
        public virtual AsignaturaDTO Asignatura { get; set; }
        public virtual DocenteDTO Docente { get; set; }
    }
}
