﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTOS;
using IServices;

namespace mvc.Controllers
{
    [Authorize(Roles = "Admin,UTP,DAEM")]
    public class DocenteController : Controller
    {
        IDocenteService _docenteService;
        public DocenteController(IDocenteService docenteService)
        {
            _docenteService = docenteService;
        }


        // GET: Docente
        public ActionResult Index()
        {
            var _docente = _docenteService.GetAll();
            return View(_docente);
        }

        // GET: Docente/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Docente/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Docente/Create
        [HttpPost]
        public ActionResult Create(DocenteDTO docente)
        {
            try
            {
                // TODO: Add insert logic here
                docente.Estado = "A";
                docente.FechaModificacion = DateTime.Now;
                docente.IdUsuario = 1;
                if (_docenteService.Create(docente))
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return View();
                }
            }
            catch(Exception e )
            {

                return View();
            }
        }

        // GET: Docente/Edit/5
        public ActionResult Edit(int id)
        {
            var docente = _docenteService.GetById(id);
            return View(docente);
        }

        // POST: Docente/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, DocenteDTO docente)
        {
            try
            {
                // TODO: Add update logic here
                docente.Estado = "A";
                docente.FechaModificacion = DateTime.Now;
                docente.IdUsuario = 1;
                if (_docenteService.Update(docente)) {
                    return RedirectToAction("Index");
                }
                else {
                    return View();
                }
            }
            catch
            {
                return View();
            }
        }

        // GET: Docente/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Docente/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
