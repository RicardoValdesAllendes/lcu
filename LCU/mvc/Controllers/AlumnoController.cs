﻿using DTOS;
using IServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace mvc.Controllers
{
    [Authorize(Roles = "Admin,UTP,DAEM")]
    public class AlumnoController : Controller
    {
        IAlumnoService _alumnoService;
        ICursoService _cursoService;
        IAlumnoCursoService _alumnoCursoService;
        public AlumnoController(IAlumnoService alumnoService, ICursoService cursoService, IAlumnoCursoService alumnoCursoService)
        {
            _alumnoService = alumnoService;
            _cursoService = cursoService;
            _alumnoCursoService = alumnoCursoService;
        }
        // GET: Alumno
        public ActionResult Index()
        {
            //var _alumno = _alumnoService.GetAll();
            return View();
        }
        [HttpGet]
        public JsonResult getAllAlumnos()
        {
            var alumno = _alumnoService.GetAll();
            return Json(alumno, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);

        }

        // GET: Alumno/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Alumno/Create
        public ActionResult Create()
        {
            ViewBag.cursos = _cursoService.GetAll();
            return View();
        }

        // POST: Alumno/Create
        [HttpPost]
        public ActionResult Create(AlumnoDTO alumno, string curso)
        {
            try
            {
                // TODO: Add insert logic here
                alumno.Estado = "A";
                alumno.FechaModificacion = DateTime.Now;
                alumno.IdUsuario = 1;
                if (_alumnoService.Create(alumno))
                {
                    var nvoAlumno = _alumnoService.GetAll().LastOrDefault();
                    AlumnoCursoDTO _alumnoCursoDTO = new AlumnoCursoDTO();
                    CursoDTO _cursoDto = new CursoDTO();
                    _cursoDto = _cursoService.GetById(int.Parse(curso));
                    alumno.Id = nvoAlumno.Id;
                    _alumnoCursoDTO.Alumno = alumno;
                    _alumnoCursoDTO.Año = DateTime.Now.Year;
                    _alumnoCursoDTO.Curso = _cursoDto;
                    _alumnoCursoDTO.Estado = "A";
                    _alumnoCursoDTO.FechaModificacion = DateTime.Now;
                    _alumnoCursoDTO.IdUsuario = 1;
                    if (_alumnoCursoService.Create(_alumnoCursoDTO))
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return View();
                }

            }
            catch (Exception e)
            {
                return View();
            }
        }

        // GET: Alumno/Edit/5
        public ActionResult Edit(int id)
        {
            var alumno = _alumnoService.GetById(id);
            ViewBag.cursos = _cursoService.GetAll();
            ViewBag.cursoSel = _alumnoCursoService.GetAlumnoCursoByIds(id, DateTime.Now.Year).Curso.Id;
            return View(alumno);
        }

        // POST: Alumno/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, AlumnoDTO alumno, string curso)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    // TODO: Add update logic here
                    alumno.Estado = "A";
                    alumno.FechaModificacion = DateTime.Now;
                    alumno.IdUsuario = 1;
                    if (_alumnoService.Update(alumno))
                    {
                        var _cursoDto = _cursoService.GetById(int.Parse(curso));
                        var _alumnoCursoDTO = _alumnoCursoService.GetAlumnoCursoByIds(alumno.Id, DateTime.Now.Year);
                        _alumnoCursoDTO.Alumno = alumno;
                        _alumnoCursoDTO.Curso = _cursoDto;
                        _alumnoCursoDTO.Estado = "A";
                        _alumnoCursoDTO.FechaModificacion = DateTime.Now;
                        _alumnoCursoDTO.IdUsuario = 1;
                        
                        if (_alumnoCursoService.Update(_alumnoCursoDTO))
                        {
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            return View();
                        }
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return View();
                }
            }
            catch (Exception e)
            {
                var msg = e;
                ViewBag.cursos = _cursoService.GetAll();
                return View();
            }
        }

        // GET: Alumno/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Alumno/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
