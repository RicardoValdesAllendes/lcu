﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTOS;
using IServices;

namespace mvc.Controllers
{
    [Authorize(Roles = "Admin,UTP,DAEM")]
    public class CursosController : Controller
    {
        ICursoService _cursosService;

        public CursosController(ICursoService cursoService)
        {
            _cursosService = cursoService;
        }
        // GET: Cursos
        public ActionResult Index()
        {
            var _curso = _cursosService.GetAll();
            return View(_curso);
        }

        // GET: Cursos/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Cursos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Cursos/Create
        [HttpPost]
        public ActionResult Create(CursoDTO vCurso)
        {
            try
            {
                // TODO: Add insert logic here
                vCurso.FechaModificacion = DateTime.Now;
                vCurso.Estado = "A";
                vCurso.IdUsuario = 1;

                if (_cursosService.Create(vCurso))
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return View();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return View();
            }
        }

        // GET: Cursos/Edit/5
        public ActionResult Edit(int id)
        {
            var curso = _cursosService.GetById(id);
            return View(curso);
        }

        // POST: Cursos/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, CursoDTO vcurso)
        {
            try
            {

                // TODO: Add update logic here
                vcurso.Estado = "A";
                vcurso.FechaModificacion = DateTime.Now;
                vcurso.IdUsuario = 1;
                vcurso.Id = id;
                if (_cursosService.Update(vcurso))
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return View();
                }
            


            }
            catch (Exception e)
            {
                var msg = e;
                return View();
    }
}

// GET: Cursos/Delete/5
public ActionResult Delete(int id)
{
    return View();
}

// POST: Cursos/Delete/5
[HttpPost]
public ActionResult Delete(int id, FormCollection collection)
{
    try
    {
        // TODO: Add delete logic here

        return RedirectToAction("Index");
    }
    catch
    {
        return View();
    }
}
    }
}
