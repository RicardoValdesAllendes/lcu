﻿using IServices;
using mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
namespace mvc.Controllers
{
    [Authorize(Roles = "Admin,UTP,DAEM")]
    public class InformesController : Controller
    {
        ICursoService _cursoService;
        IPruebaService _pruebaService;
        IReporteService _reporteService;

        public InformesController(ICursoService cursoService, IPruebaService pruebaService, IReporteService reporteService)
        {
            _cursoService = cursoService;
            _pruebaService = pruebaService;
            _reporteService=reporteService;
        }
        
        public ActionResult Index()
        {
            return View();
        }
        // GET: Informes/Create
        public ActionResult Create()
        {
            ViewBag.cursos = _cursoService.GetAll();
            ViewBag.pruebas = _pruebaService.GetAll();
            return View();
        }

        // GET: Informes/Create
        public ActionResult CreateAlumnoEje()
        {
            ViewBag.cursos = _cursoService.GetAll();
            ViewBag.pruebas = _pruebaService.GetAll();
            return View();
        }
        // GET: Informes/Create
        public ActionResult CreateEjePrueba()
        {
            ViewBag.cursos = _cursoService.GetAll();
            ViewBag.pruebas = _pruebaService.GetAll();
            return View();
        }

        [HttpGet]
        public JsonResult GetInformeByAlumno(int periodo, int curso, int prueba)
        {
            var resultado = _reporteService.getResultadoByAlumno(periodo, curso, prueba);
            return Json(resultado, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult getInformeByAlumnoEje(int periodo, int curso, int prueba)
        {
            var resultado = _reporteService.getResultadoByAlumnoEje(periodo, curso, prueba);
            return Json(resultado, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult getInformeByEjePrueba(int periodo, int curso, int prueba)
        {
            var resultado = _reporteService.getResultadoByEjePrueba(periodo, curso, prueba);
            return Json(resultado, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
    }
}
