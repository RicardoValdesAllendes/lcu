﻿using IServices;
using mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace mvc.Controllers
{
    [Authorize(Roles = "Admin,UTP,DAEM")]
    public class RendicionController : Controller
    {
        IPruebaService _pruebaService;
        IRendicionService _rendicionService;
        IResultadoService _resultadoService;
        ICursoService _cursoService;
        IAlumnoService _alumnoService;
        IPreguntaService _preguntaService;

         public RendicionController(IPruebaService pruebaService, IRendicionService rendicionService, 
             IResultadoService resultadoService, ICursoService cursoService,IAlumnoService alumnoService,
             IPreguntaService preguntaService)
        {
            _pruebaService = pruebaService;
            _rendicionService = rendicionService;
            _resultadoService = resultadoService;
            _cursoService = cursoService;
            _alumnoService = alumnoService;
            _preguntaService = preguntaService;
        }


        // GET: Rendicion
        public ActionResult Index()
        {
            var rendicion = _rendicionService.GetAll();
            return View(rendicion);
        }

        // GET: Rendicion/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Rendicion/Create
        public ActionResult Create()
        {
            ViewBag.cursos = _cursoService.GetAll();
            ViewBag.pruebas = _pruebaService.GetAll();
            return View();
        }

        // POST: Rendicion/Create
        [HttpPost]
        public ActionResult Create(List<AlumnoPruebaResultado> data)
        {
            var user = 1;
            var estado = "A";
            var ahora = DateTime.Now;
            try
            {
                foreach (var item in data)
                {
                    item.AlumnoPrueba.IdUsuario = user;
                    item.AlumnoPrueba.Estado = estado;
                    item.AlumnoPrueba.FechaModificacion = ahora;
                    //item.AlumnoPrueba.Alumno = _alumnoService.GetById(item.AlumnoPrueba.Alumno.Id);
                    //item.AlumnoPrueba.Prueba = _pruebaService.GetById(item.AlumnoPrueba.Prueba.Id);
                    if (_rendicionService.Create(item.AlumnoPrueba))
                    {
                        var idAlumnoPrueba = _rendicionService.GetAll().LastOrDefault();
                        foreach (var key in item.Resultados)
                        {
                            key.Estado = estado;
                            key.FechaModificacion = ahora;
                            key.IdUsuario = user;
                            key.AlumnoPrueba = idAlumnoPrueba;
                            //key.Pregunta = _preguntaService.GetById(key.Pregunta.Id);
                            if (_resultadoService.Create(key))
                            {
                                var ok = true;
                            }
                            else
                            {
                                return View();
                            }
                        }
                    }
                    else
                    {
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                var msg = e.Message;
                throw e;
            }
        }

        // GET: Rendicion/Edit/5
        public ActionResult Edit(int id = 0)
        {
            ViewBag.cursos = _cursoService.GetAll();
            ViewBag.pruebas = _pruebaService.GetAll();
            return View();
        }

        // POST: Rendicion/Edit/5
        [HttpPost]
        public ActionResult Edit(List<AlumnoPruebaResultado> data)
        {
            var user = 1;
            var estado = "A";
            var ahora = DateTime.Now;
            try
            {
                foreach (var item in data)
                {
                    item.AlumnoPrueba = _rendicionService.getByAlumnoPruebaIds(item.AlumnoPrueba.Alumno.Id, item.AlumnoPrueba.Prueba.Id);

                    item.AlumnoPrueba.IdUsuario = user;
                    item.AlumnoPrueba.Estado = estado;
                    item.AlumnoPrueba.FechaModificacion = ahora;
                    if (_rendicionService.Update(item.AlumnoPrueba))
                    {
                        foreach (var key in item.Resultados)
                        {
                            var nuevo = _resultadoService.GetById(key.Id);

                            if (nuevo == null) {

                                key.Estado = estado;
                                key.FechaModificacion = ahora;
                                key.IdUsuario = user;
                                key.AlumnoPrueba = item.AlumnoPrueba;
                                nuevo = key;
                            }
                            else
                            {
                                

                                nuevo.Estado = estado;
                                nuevo.FechaModificacion = ahora;
                                nuevo.IdUsuario = user;
                                nuevo.AlumnoPrueba = item.AlumnoPrueba;
                                nuevo.Respuesta = key.Respuesta;
                            }
                            
                            if (_resultadoService.Update(nuevo))
                            {
                                var ok = true;
                            }
                            else
                            {
                                return View();
                            }
                        }
                    }
                    else
                    {
                        return View();
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                var msg = e.Message;
                throw e;
            }
        }

        // GET: Rendicion/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Rendicion/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public JsonResult getAlumnosByCurso(int id)
        {
            var alumnos = _alumnoService.GetAlumnosbyCurso(id);
            return Json(alumnos, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult getPreguntasByPrueba(int id)
        {
            var preguntas = _preguntaService.GetPreguntasByPrueba(id);
            return Json(preguntas, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult getRespuestaByPreguntaAlumnoPrueba(int p, int a, int t)
        {
            var pregunta = _resultadoService.getRespuestaByPreguntaAlumnoPrueba(p,a,t);
            List<string> respuesta = new List<string>();
            if (pregunta != null)
            {
                respuesta.Add(pregunta.Respuesta);
                respuesta.Add("" + pregunta.AlumnoPrueba.Puntaje);
                respuesta.Add("" + pregunta.AlumnoPrueba.Nota);
                respuesta.Add("" + pregunta.Id);
            }
            else
            {
                respuesta.Add("");
                respuesta.Add("");
                respuesta.Add("");
                respuesta.Add("");
            }
            return Json(respuesta, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
    }
}
