﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DTOS;
using IServices;

namespace mvc.Controllers
{
    [Authorize(Roles = "Admin,UTP,DAEM")]
    public class AsignaturaController : Controller
    {
        IAsignaturaService _asignaturaService;
        public AsignaturaController(IAsignaturaService asignaturaService)
        {
            _asignaturaService = asignaturaService;
        }
        // GET: Asignatura
        public ActionResult Index()
        {
            var _asignatura = _asignaturaService.GetAll();
            return View(_asignatura);
        }

        // GET: Asignatura/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Asignatura/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Asignatura/Create
        [HttpPost]
        public ActionResult Create(AsignaturaDTO asignatura)
        {
            try
            {
                // TODO: Add insert logic here
                asignatura.Estado = "A";
                asignatura.FechaModificacion = DateTime.Now;
                asignatura.IdUsuario = 1;
                if (_asignaturaService.Create(asignatura))
                {
                    return RedirectToAction("Index");
                }
                else {
                    return View();
                }

                
            }
            catch
            {
                return View();
            }
        }

        // GET: Asignatura/Edit/5
        public ActionResult Edit(int id)
        {
            var asignatura = _asignaturaService.GetById(id);
            return View(asignatura);
        }

        // POST: Asignatura/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, AsignaturaDTO asignatura)
        {
            try
            {
                // TODO: Add update logic here
                asignatura.Estado = "A";
                asignatura.FechaModificacion = DateTime.Now;
                asignatura.IdUsuario = 1;
                if (_asignaturaService.Update(asignatura))
                {
                    return RedirectToAction("Index");
                }
                else {
                    return View();
                }
                
            }
            catch
            {
                return View();
            }
        }

        // GET: Asignatura/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Asignatura/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
