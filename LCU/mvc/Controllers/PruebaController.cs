﻿using IServices;
using mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mvc.Controllers
{
    [Authorize(Roles = "Admin,UTP,DAEM")]
    public class PruebaController : Controller
    {
        IPruebaService _pruebaService;
        IPreguntaService _preguntaService;

        public PruebaController(IPruebaService pruebaService, IPreguntaService preguntaService)
        {
            _pruebaService = pruebaService;
            _preguntaService = preguntaService;
        }

        // GET: Prueba
        public ActionResult Index()
        {
            var pruebas = _pruebaService.GetAll().OrderBy(x=>x.Id);
            return View(pruebas);
        }

        // GET: Prueba/Details/5
        public ActionResult Details(int id)
        {
            var prueba = _pruebaService.GetById(id);
            return View(prueba);
        }

        // GET: Prueba/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Prueba/Create
        [HttpPost]
        public ActionResult Create(PruebaPreguntas pp)
        {
            try
            {
                // Insertar en orden;
                //primero la prueba, lo que devuelve el id, luego ese id se agrega a las preguntas y finalmente se agregan las preguntas
                var user = 1;
                var estado = "A";
                var ahora = DateTime.Now;
                pp.Prueba.IdUsuario = user;
                pp.Prueba.Estado = estado;
                pp.Prueba.FechaModificacion = ahora;

                if (_pruebaService.Create(pp.Prueba))
                {
                    var id = _pruebaService.GetAll().LastOrDefault();
                    foreach (var item in pp.Preguntas)
                    {
                        item.Prueba = id;
                        item.IdUsuario = user;
                        item.Estado = estado;
                        item.FechaModificacion = ahora;
                    }
                    if (_preguntaService.Create(pp.Preguntas))
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View();
                    }
                }
                else
                {
                    return View();
                }
            }
            catch (Exception e)
            {
                return View();
            }
        }

        // GET: Prueba/Edit/5
        public ActionResult Edit(int id)
        {
            var prueba = _pruebaService.GetById(id);
            ViewBag.preguntas = _preguntaService.GetPreguntasByPrueba(id);
            return View(prueba);
        }

        // POST: Prueba/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, PruebaPreguntas pp)
        {
            try
            {
                var user = 1;
                var estado = "A";
                var ahora = DateTime.Now;

                var prueba = _pruebaService.GetById(id);
                prueba.IdUsuario = user;
                prueba.FechaModificacion = ahora;
                foreach (var item in pp.Preguntas)
                {
                    item.Prueba = prueba;
                    item.IdUsuario = user;
                    item.Estado = estado;
                    item.FechaModificacion = ahora;
                }
                _preguntaService.Update(pp.Preguntas);
                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                var prueba = _pruebaService.GetById(id);
                ViewBag.preguntas = _preguntaService.GetPreguntasByPrueba(id);
                return View(prueba);
            }
        }

        // GET: Prueba/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Prueba/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
