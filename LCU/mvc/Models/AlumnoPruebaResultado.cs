﻿using DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mvc.Models
{
    public class AlumnoPruebaResultado
    {
        public AlumnoPruebaDTO AlumnoPrueba { get; set; }
        public List<ResultadoDTO> Resultados { get; set; }
    }
}   