﻿using DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mvc.Models
{
    public class PruebaPreguntas
    {
        public PruebaDTO Prueba { get; set; }
        public List<PreguntaDTO> Preguntas { get; set; }
    }
}